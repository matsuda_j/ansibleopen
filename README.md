# ansible playbooks

## 概要
rolesを組み合わせてサーバーの構築・更新を行います

### ディレクトリ構造
ansible  
┗ playbook  
　　┣ host_vars : サーバー毎の主にtemplateやtasksで利用する変数を設定  
　　┣ hosts : サーバーの基本情報を設定  
　　┣ keys : ansibleでSSHログインする際の秘密鍵を保存  
　　┣ roles : 実際に実施される処理を記載  
　　┗ *.yml : サーバー毎にrolesをまとめたファイル(base.ymlは除く)  

### 共通
- 初回にbase.ymlを実施する際はリモートサーバー側で公開鍵の設定が行われていないのでrootユーザーでパスワードを利用してログインする
- base.ymlを実行したサーバーはrootユーザーでのsshログインができなくなっているのでdeployerユーザーで公開鍵でログインする
- 事前に `chmod 600 ./keys/id_deployer` を実行する

## コマンドサンプル
### サーバーの基本的な構築をatlassianサーバーで行う(パスワード認証)
```
ansible-playbook ./base.yml -i ./hosts/atlassian --ask-pass -u root
```

### atlassianのソフトウェアのインストールをatlassianサーバーで行う(公開鍵認証)
```
ansible-playbook ./local_atlassian.yml -i ./hosts/local_atlassian  -u deployer --private-key=./keys/id_deployer
```

### postgresqlのインストールだけをatlassianサーバーで行う(公開鍵認証)
```
ansible-playbook ./local_atlassian.yml -i ./hosts/local_atlassian  -u deployer --private-key=./keys/id_deployer --tags "postgresql"
```