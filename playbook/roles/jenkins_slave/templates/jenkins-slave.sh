#!/bin/bash
case "$1" in
  start)
    /usr/local/java/bin/java -jar {{ jenkins_home }}/slave.jar -jnlpUrl {{ jenkins_master_agent_url }} -secret {{ jenkins_master_secret }} > /var/log/jenkins/jenkins_slave.log 2>&1 &
    /usr/local/java/bin/jps | grep slave.jar | cut -d " " -f 1 > {{ jenkins_home }}/jenkins.pid
    ;;
  stop)
    kill `cat {{ jenkins_home }}/jenkins.pid`
    rm -f {{ jenkins_home }}/jenkins.pid
    ;;
  *)
    echo $"Usage: /etc/init.d/jenkins_slave {start|stop}"
esac