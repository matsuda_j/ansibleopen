#!/bin/sh
for table in `sudo su - postgres -c "/usr/local/pgsql/bin/psql -qAt -c \"select tablename from pg_tables where schemaname = 'public';\" zabbix -P pager=off"`
do
  sudo su - postgres -c "/usr/local/pgsql/bin/psql -c \"alter table ${table} owner to zabbix;\" zabbix -P pager=off";
done
