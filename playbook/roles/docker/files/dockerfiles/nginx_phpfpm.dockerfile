FROM centos:6
RUN echo 'options ipv6 disable=1' >> /etc/modprobe.d/ipv6.conf
RUN echo 'include_only=.jp' >> /etc/yum/pluginconf.d/fastestmirror.conf
RUN echo 'NETWORKING_IPV6=no' >> /etc/sysconfig/network
RUN yum clean all && yum repolist
RUN yum -y install epel-release && yum clean all
RUN rpm -ivh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
RUN rpm -ivh http://nginx.org/packages/centos/6/noarch/RPMS/nginx-release-centos-6-0.el6.ngx.noarch.rpm

RUN yum -y update && yum clean all
RUN yum --enablerepo=remi --enablerepo=remi-php56 -y install php-opcache supervisor git nginx php php-fpm php-mbstring php-mysql php-gd unzip wget haproxy ntp curl sudo php-pecl-memcached && yum clean all

RUN mkdir -p /var/www/current
#RUN git clone https://bitbucket.org/matsuda_j/phpfiles.git /var/www/current

RUN rm -f /etc/nginx/nginx.conf
ADD ./conf/nginx/nginx.conf /etc/nginx/
RUN chmod 644 /etc/nginx/nginx.conf

RUN rm -fr /etc/nginx/conf.d
ADD ./conf/nginx/conf.d /etc/nginx/conf.d
RUN chmod 755 /etc/nginx/conf.d
RUN chmod 644 /etc/nginx/conf.d/*

RUN rm -f /etc/supervisord.conf
ADD ./conf/supervisor/nginx-supervisord.conf /etc/supervisord.conf
RUN chmod 644 /etc/supervisord.conf

RUN rm -f /etc/php.ini
ADD ./conf/php/php.ini /etc/php.ini
RUN chmod 644 /etc/php.ini

RUN rm -f /etc/php-fpm.d/www.conf
ADD ./conf/phpfpm/www.conf /etc/php-fpm.d/www.conf
RUN chmod 644 /etc/php-fpm.d/www.conf

RUN rm -f /etc/haproxy/haproxy.cfg
ADD ./conf/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg
RUN chmod 644 /etc/haproxy/haproxy.cfg

RUN rm -f /etc/php.d/50-memcached.ini
ADD ./conf/php/50-memcached.ini /etc/php.d/50-memcached.ini
RUN chmod 644 /etc/php.d/50-memcached.ini

RUN cp -f /usr/share/zoneinfo/Japan /etc/localtime
RUN ntpdate ntp.nict.jp
RUN rm -f /etc/ntp.conf
ADD ./conf/ntp/ntp.conf /etc/ntp/ntp.conf
RUN chmod 644 /etc/ntp/ntp.conf

RUN rm -f /etc/limits.conf
ADD ./conf/ulimit/limits.conf /etc/limits.conf

RUN echo 'Defaults:root !requiretty' >> /etc/sudoers

RUN curl -L http://toolbelt.treasuredata.com/sh/install-redhat.sh | sh
RUN rm -f /etc/td-agent/td-agent.conf
ADD ./conf/fluentd/td-agent.conf /etc/td-agent/td-agent.conf
RUN chmod 644 /etc/td-agent/td-agent.conf
RUN gpasswd -a td-agent adm

RUN chkconfig nginx off
RUN chkconfig php-fpm off
RUN chkconfig haproxy off
RUN chkconfig ntpd off
RUN chkconfig td-agent off

EXPOSE 9090
#EXPOSE 9090/udp
CMD /usr/bin/supervisord -c /etc/supervisord.conf