haproxy_frontend:
  - acl: [
    "phpfiles hdr(host) -i php.matsuda.com",
    "consul hdr(host) -i consul.matsuda.com"
  ]
  - use_backend: [
    "php_http_backend if phpfiles",
    "consul_http_backend if consul"
  ]
haproxy_backend:
  - backend1 : {
    name : php_http_backend,
    order_params: [
      "mode http",
      "option httplog",
      "option forwardfor",
      "option http-server-close",
      "balance roundrobin",
      "{% raw %}{{range service \"nginx_phpfpm\"}}{% endraw %}",
      "{% raw %}server {{.ID}} {{with node}}{{.Node.Address}}{{end}}:{{.Port}} check inter 10000 rise 2 fall 5{{end}}{% endraw %}"
    ]
  }
  - backend2 : {
    name : consul_http_backend,
    order_params: [
      "mode http",
      "option httplog",
      "option forwardfor",
      "option http-server-close",
      "balance roundrobin",
      "server consul01:8500 127.0.0.1:8500 check inter 10000 rise 2 fall 5"
    ]
  }

firewalld_setting: 
  - firewall-cmd --add-port=80/tcp --zone=public --permanent
  - firewall-cmd --add-port=443/tcp --zone=public --permanent
  - firewall-cmd --add-source=192.168.33.102/24 --zone=trusted --permanent
  - firewall-cmd --add-source=172.17.0.0/24 --zone=trusted --permanent
  - firewall-cmd --add-source=172.16.0.0/24 --zone=trusted --permanent

consul_type: server
consul_master_ip: 192.168.33.101
install_docker_nginx_phpfpm : "yes"

sysctl_vars:
  - {name: "net.ipv4.tcp_syncookies", value: "1"}
  - {name: "net.ipv4.icmp_ignore_bogus_error_responses", value: "1"}
  - {name: "net.ipv4.conf.all.log_martians", value: "1"}
  - {name: "net.core.wmem_max", value: "1048576"}
  - {name: "net.core.rmem_max", value: "1048576"}
  - {name: "net.core.optmem_max", value: "20480"}
  - {name: "net.ipv4.tcp_keepalive_time", value: "1800"}
  - {name: "net.ipv4.tcp_rmem", value: "4096 873800 1747600"}
  - {name: "net.ipv4.tcp_timestamps", value: "0"}
  - {name: "kernel.panic", value: "60"}
  - {name: "net.ipv4.conf.default.rp_filter", value: "1"}
  - {name: "net.ipv4.conf.default.accept_source_route", value: "0"}
  - {name: "kernel.sysrq", value: "0"}
  - {name: "kernel.core_uses_pid", value: "1"}
  - {name: "kernel.msgmnb", value: "65536"}
  - {name: "kernel.msgmax", value: "65536"}
  - {name: "kernel.shmmax", value: "68719476736"}
  - {name: "kernel.shmall", value: "4294967296"}
  - {name: "net.ipv6.conf.all.disable_ipv6", value: "1"}
  - {name: "net.ipv6.conf.default.disable_ipv6", value: "1"}
  - {name: "net.ipv4.tcp_tw_recycle", value: "1"}
  - {name: "net.ipv4.tcp_tw_reuse", value: "1"}
  - {name: "net.ipv4.tcp_fin_timeout", value: "30"}
  - {name: "net.ipv4.tcp_max_syn_backlog", value: "30720"}
  - {name: "net.core.somaxconn", value: "30720"}
  - {name: "net.core.netdev_max_backlog", value: "30720"}

hosts_allow_vars:
  - "ALL : 192.168. 127.0.0.1 172.16. 172.17."

hosts_deny_vars:
  - "ALL : ALL"

database_haproxy:
  - {name: "db1", ip: "192.168.33.101", port: "3306"}
#  - {name: "db1", ip: "192.168.33.111", port: "3306"}
#  - {name: "db2", ip: "192.168.33.112", port: "3306"}
#  - {name: "db3", ip: "192.168.33.113", port: "3306"}

php_session_save_path: "192.168.33.101:11211"


nginx_worker_processes: 4
nginx_worker_connections: 1024

nginx_sites:
- server:
   file_name: php_matsuda.conf
   listen: 9090
   server_name: php.matsuda.com
   root: "/var/www/current/public"

   charset: "utf-8"
   index: "index.php"
   order_params: [
     "access_log /var/log/nginx/access.log ltsv",
     "error_log /var/log/nginx/error.log"
   ]

   location1: {
     name: '/',
     try_files: '$uri $uri/ @handler'
   }

   location2: {
     name: '@handler',
     rewrite: '^ /index.php?/$request_uri'
   }

   location3: {
     name: '~ \.php$',
     order_params: [
       'fastcgi_split_path_info ^(.+\.php)(/.+)$',
       'fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock',
       'fastcgi_index index.php',
       'fastcgi_param  SCRIPT_FILENAME  $document_root/$fastcgi_script_name',
       'include fastcgi_params'
     ]
   }

fluentd_master_server_ip: 192.168.33.101
fluentd_master_server_port: 24224

## mariadb
## mariadb
mariadb_install_type: "galera"
mariadb_create_users: True
mariadb_srcdir: "/usr/local/mariadb"
mariadb_basedir: "/usr/local/mysql"
mariadb_datadir: "/data/mysql"
mariadb_version: "10.1.12"
mariadb_source_url: "https://www.dropbox.com/s/gei05mq8j7owovt/mariadb-10.1.12-linux-x86_64.tar.gz?dl=1"

### user settings
mariadb_root_pass: *******
mariadb_dbs: 
 - name: matsuda_db
mariadb_users: 
 - name: matsuda_db_user
   password: *******
   priv: "matsuda_db.*:ALL"
   host: 192.168.33.%

### my.cnf settings
mariadb_wsrep_on: "ON"
mariadb_innodb_buffer_pool_size: 128M
mariadb_max_connections: 200 
mariadb_wsrep_cluster_address: 192.168.33.112
mariadb_wsrep_node_address: 192.168.33.101
mariadb_wsrep_provider: "/usr/local/mysql/lib/galera/libgalera_smm.so"
mariadb_server_id: 1


## mail settiongs
root_mail_forward_address: 
root_mail_from_address: matsuda.com