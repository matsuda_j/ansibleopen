
## users
# openssl passwd -salt *********** -1 *******
users:
  - username: postgres
    groups: []
    uid: 5001
    name: postgres user
    password: ""
    shell: "/bin/bash"
    ssh_key:
      - ""
  - username: mysql
    groups: []
    uid: 5002
    name: mysql user
    password: ""
    shell: "/bin/bash"
    ssh_key:
      - ""
  - username: deployer
    groups: ['wheel']
    name: deployer user
    uid: 20000
    ssh_key:
      - 
## sudo
sudo_package: sudo
sudo_users: 
  - name: 'deployer'
    nopasswd: yes
sudo_defaults: []

## Firewalld
firewalld_setting: 
  - "firewall-cmd --add-port=80/tcp --zone=public --permanent"
  - "firewall-cmd --add-port=443/tcp --zone=public --permanent"
  - "firewall-cmd --add-port=22/tcp --zone=public --permanent"
  - "firewall-cmd --add-source=192.168.33.102/24 --zone=trusted --permanent"
  - "firewall-cmd --add-source=172.17.0.0/24 --zone=trusted --permanent"
  - "firewall-cmd --add-source=172.16.0.0/24 --zone=trusted --permanent"

## sysctl
sysctl_vars:
  - {name: "net.ipv4.tcp_syncookies", value: "1"}
  - {name: "net.ipv4.icmp_ignore_bogus_error_responses", value: "1"}
  - {name: "net.ipv4.conf.all.log_martians", value: "1"}
  - {name: "net.core.wmem_max", value: "1048576"}
  - {name: "net.core.rmem_max", value: "1048576"}
  - {name: "net.core.optmem_max", value: "20480"}
  - {name: "net.ipv4.tcp_keepalive_time", value: "1800"}
  - {name: "net.ipv4.tcp_rmem", value: "4096 873800 1747600"}
  - {name: "net.ipv4.tcp_timestamps", value: "0"}
  - {name: "kernel.panic", value: "60"}
  - {name: "net.ipv4.conf.default.rp_filter", value: "1"}
  - {name: "net.ipv4.conf.default.accept_source_route", value: "0"}
  - {name: "kernel.sysrq", value: "0"}
  - {name: "kernel.core_uses_pid", value: "1"}
  - {name: "kernel.msgmnb", value: "65536"}
  - {name: "kernel.msgmax", value: "65536"}
  - {name: "kernel.shmmax", value: "68719476736"}
  - {name: "kernel.shmall", value: "4294967296"}
  - {name: "net.ipv6.conf.all.disable_ipv6", value: "1"}
  - {name: "net.ipv6.conf.default.disable_ipv6", value: "1"}
  - {name: "net.ipv4.tcp_tw_recycle", value: "1"}
  - {name: "net.ipv4.tcp_tw_reuse", value: "1"}
  - {name: "net.ipv4.tcp_fin_timeout", value: "30"}
  - {name: "net.ipv4.tcp_max_syn_backlog", value: "30720"}
  - {name: "net.core.somaxconn", value: "30720"}
  - {name: "net.core.netdev_max_backlog", value: "30720"}

## mail settiongs
root_mail_forward_address: 
root_mail_from_address: 

## nginx
nginx_repo_url: http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm

## postgresql
postgresql_source_url: https://www.dropbox.com/s/oz0cnqxhkp9gtw5/postgresql-9.5.1.tar.gz?dl=1
postgresql_file_name: 
postgres_user_pass: 
pgsql_users:
  - {db: 'jira', user: 'jirauser', pass: '***********'}
  - {db: 'confluence', user: 'confluenceuser', pass: '**********'}
  - {db: 'crowd', user: 'crowduser', pass: '********:'}

postgres_max_connections: 200
postgres_shared_buffers: "512MB"
postgres_work_mem: "8MB"
postgres_maintenance_work_mem: "128MB"
postgres_wal_buffers: "-1"
postgres_effective_cache_size: "2GB"
postgres_data_directory: /var/atlassian/postgres
postgres_bk_cron_user: postgres
postgres_bk_cron_hour: 17
postgres_bk_cron_minute: 1
postgres_bk_days: 30

## Atlassian Common
atlassian_data_directory: /var/atlassian/application_data


## jira
jira_source_url: https://www.dropbox.com/s/t68jkdkxvtwmx53/atlassian-jira-core-7.1.8.tar.gz?dl=1
jira_version: "7.1.8"
jira_home: /usr/local/atlassian/jira
jira_bk_cron_user: root
jira_bk_cron_hour: 16
jira_bk_cron_minute: 50
jira_bk_days: 30

## confluence
confluence_source_url: https://www.dropbox.com/s/1vrspleaitkrgeu/atlassian-confluence-5.10.0.tar.gz?dl=1
confluence_version: "5.10.0"
confluence_home: /usr/local/atlassian/confluence
confluence_bk_cron_user: root
confluence_bk_cron_hour: 16
confluence_bk_cron_minute: 51
confluence_bk_days: 30

## crowd
crowd_source_url: https://www.dropbox.com/s/m6q07du54611t9l/atlassian-crowd-2.8.4.tar.gz?dl=1
crowd_version: "2.8.4"
crowd_home: /usr/local/atlassian/crowd
crowd_bk_cron_user: root
crowd_bk_cron_hour: 16
crowd_bk_cron_minute: 52
crowd_bk_days: 30

## JAVA
jdk_source_url: https://www.dropbox.com/s/6avt3yv66bv4mzt/jdk-8u73-linux-x64.tar.gz?dl=1
jdk_source_name: jdk-8u73-linux-x64
jdk_version: 1.8.0_73

## SSHD
sshd_skip_defaults: false
sshd:
  PasswordAuthentication: no
  PermitRootLogin: no
  PermitEmptyPasswords: no
  ChallengeResponseAuthentication: no

## logrotate
logrotate_scripts:
  - name: nginx
    path: /var/log/nginx/*.log
    options:
      - daily
      - size 25M
      - rotate 90
      - missingok
      - compress
      - delaycompress
      - copytruncate
    scripts:
       postrotate: "[ ! -f /var/run/nginx.pid ] || kill -USR1 `cat /var/run/nginx.pid`"

## mariadb
mariadb_install_type: "server"
mariadb_create_users: True
mariadb_srcdir: "/usr/local/mariadb"
mariadb_basedir: "/usr/local/mysql"
mariadb_datadir: "/data/mysql"
mariadb_version: "10.1.12"
mariadb_source_url: "https://www.dropbox.com/s/gei05mq8j7owovt/mariadb-10.1.12-linux-x86_64.tar.gz?dl=1"

### user settings
mariadb_root_pass: *******
mariadb_dbs: 
 - name: matsuda_db
mariadb_users: 
 - name: matsuda_db_user
   password: ********
   priv: "matsuda_db.*:ALL"
   host: 192.168.33.%

### my.cnf settings
mariadb_wsrep_on: "OFF"
mariadb_innodb_buffer_pool_size: 128M
mariadb_max_connections: 200 
mariadb_wsrep_cluster_address: 192.168.33.101
mariadb_wsrep_node_address: 192.168.33.102
mariadb_wsrep_provider: "/usr/local/mysql/lib/galera/libgalera_smm.so"
mariadb_server_id: 1


## php
php_version: 7.0.4
php_source_url: "https://www.dropbox.com/s/ei9q078fhktcctw/php-7.0.4.tar.gz?dl=1"
php_home: "/usr/local/php"

## php-fpm
php_session_save_path: "192.168.33.102:11211" 

## hosts
hosts_vars:
  - "192.168.33.102    atlassian atlassian.localdomain"

## hostname
hostname: local_atlassian